﻿using Nest;
using System;
using System.Configuration;

namespace SongIndexing
{
    public class ElasticsearchConfig
    {
        public IElasticClient Client { get; set; }

        public ElasticsearchConfig()
        {
            var url = ConfigurationManager.AppSettings["elastic-node-url"];

            var node = new Uri(url);

            var settings = new ConnectionSettings(node);

            Client = new ElasticClient(settings);
        }
    }
}
