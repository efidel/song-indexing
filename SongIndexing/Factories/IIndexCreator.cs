﻿using Nest;

namespace SongIndexing
{
    public interface IIndexCreator
    {
        void Create(IElasticClient client, string indexName);
    }
}
