﻿using Nest;
using SongIndexing.Indices;

namespace SongIndexing.Factories
{
    public class SongIndexCreator : IIndexCreator
    {
        public void Create(IElasticClient client, string indexName)
        {
            if (client.IndexExists(indexName).Exists)
            {
                client.DeleteIndex(indexName);
            }

            client.CreateIndex(indexName, i => i
                .Settings(s => s
                    .NumberOfShards(2)
                    .NumberOfReplicas(0)
                    .Analysis(a => a
                        .Analyzers(l => l
                            .Custom("standard", c => c
                                .Tokenizer("standard")
                                .Filters("lowercase", "asciifolding", "standard")
                            )
                        )
                    )
                )
                .Mappings(m => m
                    .Map<Song>(map => map
                        .Properties(p => p
                            .Completion(c => c
                                .Name(n => n.Autocomplete)
                            )
                        )
                        .AutoMap()
                    )
                )
            );
        }
    }
}
