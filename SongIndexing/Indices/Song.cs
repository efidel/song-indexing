﻿using Nest;

namespace SongIndexing.Indices
{
    [ElasticsearchType(IdProperty = "SongId")]
    public class Song
    {
        public int SongId { get; set; }

        public string Title { get; set; }

        public string NormalTitle { get; set; }

        public int DownloadCount { get; set; }

        public CompletionField Autocomplete { get; set; }
    }
}
