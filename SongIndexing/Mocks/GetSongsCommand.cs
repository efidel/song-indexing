﻿using SongIndexing.Indices;
using System.Collections.Generic;

namespace SongIndexing.Mocks
{
    public class GetSongsCommand : IGetCommand<Song>
    {
        public IEnumerable<Song> Execute()
        {
            var songs = new List<Song>
            {
                new Song
                {
                    SongId = 1493,
                    Title = "Bad Bunny ft J Balvin ft Prince Royce – Sensualidad",
                    NormalTitle = "Sensualidad"
                },
                new Song
                {
                    SongId = 9901,
                    Title = "Natti Natasha ft Ozuna – Criminal",
                    NormalTitle = "Criminal"
                },
                new Song
                {
                    SongId = 41391,
                    Title = "Ozuna Ft. Romeo Santos – El Farsante (Remix)",
                    NormalTitle = "El Farsante (Remix)"
                }
            };

            return songs;
        }
    }
}
