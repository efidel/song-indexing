﻿using System.Collections.Generic;

namespace SongIndexing.Mocks
{
    public interface IGetCommand<T> where T : class
    {
        IEnumerable<T> Execute();
    }
}
