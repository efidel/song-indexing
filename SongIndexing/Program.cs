﻿using Nest;
using SongIndexing.Factories;
using SongIndexing.Mocks;
using System.Collections.Generic;
using System.Linq;

namespace SongIndexing
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var config = new ElasticsearchConfig();

            var client = config.Client;

            var songs = new GetSongsCommand().Execute();

            var indexName = Utils.Indices.Songs
                .ToString()
                .ToLower();

            //var creator = new SongIndexCreator();

            //creator.Create(client, indexName);

            var service = new SearchService(client);

            var searchs = service.SearchSongs("sensualidad");

            var autocompletes = service.AutocompleteSongs("sen");

            //foreach (var song in songs)
            //{
            //    song.Autocomplete = new CompletionField
            //    {
            //        Input = new List<string>(song.Title.Split(' '))
            //        {
            //            song.Title,
            //            song.NormalTitle
            //        },
            //        Weight = song.DownloadCount
            //    };

            //    song.Autocomplete.Input = song.Autocomplete.Input.Distinct();

            //    client.Index(song, idx => idx.Index(indexName));
            //}
        }
    }
}
