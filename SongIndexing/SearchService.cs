﻿using Nest;
using SongIndexing.Indices;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SongIndexing
{
    public class SearchService
    {
        private readonly IElasticClient _client;

        public SearchService(IElasticClient client)
        {
            _client = client;
        }

        public IEnumerable<Song> SearchSongs(string term)
        {
            var songs = _client.Search<Song>(s => s
                .Index("songs")
                .Query(q => q
                    .Bool(b => b
                        .Should(sh => sh
                            .MatchPhrase(m => m
                                .Field("normalTitle")
                                .Query(term)
                            ), sh => sh
                            .Fuzzy(f => f
                                .Field("normalTitle")
                                .Value(term)
                                .Boost(1)
                                .Fuzziness(Fuzziness.EditDistance(2))
                                .PrefixLength(3)
                                .MaxExpansions(100)
                            ), sh => sh
                            .Match(m => m
                                .Field("normalTitle")
                                .Query(term)
                            ), sh => sh
                            .MatchPhrase(m => m
                                .Field("title")
                                .Query(term)
                            ), sh => sh
                            .Fuzzy(f => f
                                .Field("title")
                                .Value(term)
                                .Boost(1)
                                .Fuzziness(Fuzziness.EditDistance(2))
                                .PrefixLength(3)
                                .MaxExpansions(100)
                            ), sh => sh
                            .Match(m => m
                                .Field("title")
                                .Query(term)
                            )
                        )
                    )
                )
                .Sort(so => so
                    .Field("downloadCount", SortOrder.Descending)
                )
            );

            return songs.Documents;
        }

        public IEnumerable<Song> AutocompleteSongs(string term)
        {
            var songs = _client.Search<Song>(s => s
                .Index("songs")
                .Suggest(su => su
                    .Completion("song-completion-suggest", c => c
                        .Prefix(term)
                        .Analyzer("standard")
                        .Field("autocomplete")
                        .Fuzzy(f => f
                            .Fuzziness(Fuzziness.EditDistance(2))
                            .MinLength(4)
                            .Transpositions(true)
                        )
                        .Size(15)
                    )
                )
            );

            var options = songs.Suggest.Values
                .FirstOrDefault()?
                .FirstOrDefault()?
                .Options?
                .Select(o => o?.Source);

            return options;
        }
    }
}
